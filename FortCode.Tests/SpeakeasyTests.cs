using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using FortCode.Repositories;
using Xunit;

namespace FortCode.Tests
{
    [Collection("Sequential")]
    public class SpeakeasyTests : IClassFixture<FortCodeSeedDataFixture>
    {
        private ISpeakeasyRepository _sut;
        private FortCodeDbContext _context;
        private ICityRepository _cityRepository;
        private User _user;
        private City _city;
        private Speakeasy _speakeasy;
        private SpeakeasyDto _speakeasyResponse;

        public SpeakeasyTests(FortCodeSeedDataFixture fixture)
        {
            _context = fixture.DbContext;

            _user = _context.Users.Single(u => u.Email == "joe@doakes.com");
            _city = _context.Cities.Single(c => c.Country == "USA");

            _sut = new SpeakeasyRepository(_context);
            _cityRepository = new CityRepository(_context);

            var speakeasyDto = new SpeakeasyDto
            {
                CityId = _city.Id,
                Name = "The New Place",
                FavoriteDrinkName = "Vodka Gimlet"
            };
            _speakeasyResponse = _sut.AddSpeakeasyAsync(speakeasyDto, _user).Result;
        }

        [Fact]
        public void Can_add_speakeasy()
        {
            Assert.NotNull(_speakeasyResponse);
            Assert.Equal(_city.Id, _speakeasyResponse.CityId);
            Assert.Equal("Vodka Gimlet", _speakeasyResponse.FavoriteDrinkName);
        }

        [Fact]
        public async Task Can_remove_speakeasy()
        {
            var response = await _sut.RemoveSpeakeasyAsync(_speakeasyResponse, _user);
            Assert.True(response);

            // must manually query the context to see if the 'UserSpeakeasy' record was removed
            var userSpeakeasy = _context.UserSpeakeasies
                .FirstOrDefault(us => us.UserId == _user.Id && us.SpeakeasyId == _speakeasyResponse.Id);

            Assert.Null(userSpeakeasy);
        }

        [Fact]
        public async Task Cannot_remove_nonexistent_speakeasy()
        {
            var response = await _sut.RemoveSpeakeasyAsync(
                new SpeakeasyDto
                {
                    Id = 9999999,
                },
                _user);
            Assert.False(response);
        }

        [Fact]
        public async Task Can_retrieve_favorite_speakeasies()
        {
            var response = await _sut.GetFavoriteSpeakeasiesInCityAsync(
                _city.Id,
                _user);

            Assert.NotEmpty(response);

            foreach (var speakeasy in response)
            {
                Assert.Equal(_city.Id, speakeasy.CityId);
                var favoriteDrinkName = speakeasy.FavoriteDrinkName;
                var userSpeakeasy = _context.UserSpeakeasies
                    .Single(us => us.SpeakeasyId == speakeasy.Id && us.UserId == _user.Id);
                Assert.Equal(favoriteDrinkName, userSpeakeasy.FavoriteDrinkName);
            }
        }

        [Fact]
        public async Task Can_find_shared_speakeasies()
        {
            var matches = await _sut.GetSharedSpeakeasiesAsync(_user);

            Assert.NotEmpty(matches);

            var enVracMatch = matches.Single(m => m.Name == "En Vrac");
            Assert.Equal(1, enVracMatch.OtherUserCount);

            var roadhouseMatch = matches.Single(m => m.Name == "The Roadhouse");
            Assert.Equal(1, enVracMatch.OtherUserCount);
        }
    }
}