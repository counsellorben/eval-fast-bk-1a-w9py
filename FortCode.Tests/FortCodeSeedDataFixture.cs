using System;
using FortCode.Models;
using FortCode.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Tests
{
    public class FortCodeSeedDataFixture : IDisposable
    {
        public FortCodeDbContext DbContext { get; private set; }

        public FortCodeSeedDataFixture()
        {
            var options = new DbContextOptionsBuilder<FortCodeDbContext>()
                .UseInMemoryDatabase($"FortCodeDb{DateTime.UtcNow.Ticks}")
                .Options;

            DbContext = new FortCodeDbContext(options);

            var joeEntity = DbContext.Users.Add(new User { Name = "Joe", Email = "joe@doakes.com", HashedPassword = "hashed1" });
            var alEntity = DbContext.Users.Add(new User { Name = "Al", Email = "al@smith.com", HashedPassword = "hashed2" });
            var janeEntity = DbContext.Users.Add(new User { Name = "Jane", Email = "jane@roe.com", HashedPassword = "hashed3" });
            DbContext.SaveChanges();

            var parisTxEntity = DbContext.Cities.Add(new City { Name = "Paris", Country = "USA" });
            var parisFranceEntity = DbContext.Cities.Add(new City { Name = "Paris", Country = "FR" });
            DbContext.SaveChanges();

            DbContext.UserCities.Add(new UserCity { UserId = joeEntity.Entity.Id, CityId = parisTxEntity.Entity.Id });
            DbContext.UserCities.Add(new UserCity { UserId = joeEntity.Entity.Id, CityId = parisFranceEntity.Entity.Id });
            DbContext.UserCities.Add(new UserCity { UserId = alEntity.Entity.Id, CityId = parisTxEntity.Entity.Id });
            DbContext.UserCities.Add(new UserCity { UserId = janeEntity.Entity.Id, CityId = parisFranceEntity.Entity.Id });
            DbContext.SaveChanges();

            var roadhouseEntity = DbContext.Speakeasies.Add(new Speakeasy { Name = "The Roadhouse", CityId = parisTxEntity.Entity.Id });
            var jacksEntity = DbContext.Speakeasies.Add(new Speakeasy { Name = "Jack's Saloon", CityId = parisTxEntity.Entity.Id });
            var enVracEntity = DbContext.Speakeasies.Add(new Speakeasy { Name = "En Vrac", CityId = parisFranceEntity.Entity.Id });
            var leTroisEntity = DbContext.Speakeasies.Add(new Speakeasy { Name = "Le Trois 8", CityId = parisFranceEntity.Entity.Id });
            DbContext.SaveChanges();

            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = roadhouseEntity.Entity.Id, UserId = joeEntity.Entity.Id, FavoriteDrinkName = "Bud" });
            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = enVracEntity.Entity.Id, UserId = joeEntity.Entity.Id, FavoriteDrinkName = "Absinthe" });
            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = roadhouseEntity.Entity.Id, UserId = alEntity.Entity.Id, FavoriteDrinkName = "Bourbon" });
            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = jacksEntity.Entity.Id, UserId = alEntity.Entity.Id, FavoriteDrinkName = "Rye" });
            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = enVracEntity.Entity.Id, UserId = janeEntity.Entity.Id, FavoriteDrinkName = "Martini" });
            DbContext.UserSpeakeasies.Add(new UserSpeakeasy { SpeakeasyId = leTroisEntity.Entity.Id, UserId = janeEntity.Entity.Id, FavoriteDrinkName = "Grappa" });
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}