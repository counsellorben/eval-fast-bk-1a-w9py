using System.Threading.Tasks;
using FortCode.Models;
using FortCode.Repositories;
using Xunit;

namespace FortCode.Tests
{
    [Collection("Sequential")]
    public class UserTests : IClassFixture<FortCodeSeedDataFixture>
    {
        private IUserRepository _sut;
        private LoginResponseDto _loginResponse;

        public UserTests(FortCodeSeedDataFixture fixture)
        {
            _sut = new UserRepository(fixture.DbContext);

            var userExists = _sut.EmailExistsAsync("betty@white.com").Result;

            if (userExists)
            {
                var login = new LoginDto
                {
                    Email = "betty@white.com",
                    Password = "betty"
                };
                _loginResponse = _sut.LoginUserAsync(login).Result;

                return;
            }

            var user = new UserDto
            {
                Name = "Betty",
                Email = "betty@white.com",
                Password = "betty"
            };
            _loginResponse = _sut.AddUserAsync(user).Result;
        }

        [Fact]
        public void Can_add_user()
        {
            Assert.NotNull(_loginResponse);
            Assert.NotNull(_loginResponse.JwtToken);
        }

        [Fact]
        public async Task Can_login_existing_user()
        {
            var login = new LoginDto
            {
                Email = "betty@white.com",
                Password = "betty"
            };
            var response = await _sut.LoginUserAsync(login);

            Assert.NotNull(response);
            Assert.NotNull(response.JwtToken);
        }

        [Fact]
        public async Task Cannot_login_existing_user_with_incorrect_password()
        {
            var login = new LoginDto
            {
                Email = "betty@white.com",
                Password = "not-betty"
            };
            var response = await _sut.LoginUserAsync(login);

            Assert.Null(response);
        }

        [Fact]
        public async Task Cannot_login_with_bad_email_address()
        {
            var login = new LoginDto
            {
                Email = "not-betty@white.com",
                Password = "betty"
            };
            var response = await _sut.LoginUserAsync(login);

            Assert.Null(response);
        }

        [Fact]
        public async Task Cannot_recreate_existing_user()
        {
            var user = new UserDto
            {
                Name = "Betty",
                Email = "betty@white.com",
                Password = "not-betty"
            };
            var response = await _sut.AddUserAsync(user);

            Assert.Null(response);
        }
    }
}
