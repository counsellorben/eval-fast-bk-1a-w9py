using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using FortCode.Repositories;
using Xunit;

namespace FortCode.Tests
{
    [Collection("Sequential")]
    public class CityTests : IClassFixture<FortCodeSeedDataFixture>
    {
        private ICityRepository _sut;
        private FortCodeDbContext _context;
        private IUserRepository _userRepository;
        private LoginResponseDto _loginResponse;
        private CityDto _cityResponse;
        private User _user;

        public CityTests(FortCodeSeedDataFixture fixture)
        {
            _sut = new CityRepository(fixture.DbContext);
            _context = fixture.DbContext;
            _userRepository = new UserRepository(fixture.DbContext);

            var userExists = _userRepository.EmailExistsAsync("betty@white.com").Result;

            if (userExists)
            {
                var login = new LoginDto
                {
                    Email = "betty@white.com",
                    Password = "betty"
                };
                _loginResponse = _userRepository.LoginUserAsync(login).Result;

                _user = _userRepository.GetUserByEmailAsync("betty@white.com").Result;

                SetupCity();

                return;
            }

            var user = new UserDto
            {
                Name = "Betty",
                Email = "betty@white.com",
                Password = "betty"
            };
            _loginResponse = _userRepository.AddUserAsync(user).Result;

            Assert.NotNull(_loginResponse);
            Assert.NotNull(_loginResponse.JwtToken);

            _user = _userRepository.GetUserByEmailAsync("betty@white.com").Result;

            SetupCity();
        }

        private void SetupCity()
        {
            var city = new CityDto
            {
                Name = "Los Angeles",
                Country = "USA"
            };
            _cityResponse = _sut.AddFavoriteCityAsync(city, _user).Result;
        }

        [Fact]
        public void Can_add_favorite_city()
        {
            Assert.NotNull(_cityResponse);
            Assert.Equal("Los Angeles", _cityResponse.Name);
            Assert.Equal("USA", _cityResponse.Country);
        }

        [Fact]
        public async Task Can_remove_favorite_city()
        {
            var city = new CityDto
            {
                Id = _cityResponse.Id,
            };
            var response = await _sut.RemoveFavoriteCityAsync(city, _user);

            Assert.True(response);

            // must manually query the context to see if the 'UserCity' record was removed
            var userCity = _context.UserCities
                .FirstOrDefault(uc => uc.UserId == _user.Id && uc.CityId == _cityResponse.Id);

            Assert.Null(userCity);
        }

        [Fact]
        public async Task Cannot_remove_nonexistent_favorite_city()
        {
            var city = new CityDto
            {
                Id = 999999,
            };
            var response = await _sut.RemoveFavoriteCityAsync(city, _user);

            Assert.False(response);
        }

        [Fact]
        public async Task Can_retrieve_favorite_cities()
        {
            var favoriteCities = await _sut.GetFavoriteCitiesAsync(_user);

            Assert.NotEmpty(favoriteCities);

            var userFavoriteCityCount = 0;
            foreach (var favoriteCity in favoriteCities)
            {
                var userCity = _context.UserCities
                    .FirstOrDefault(uc => uc.UserId == _user.Id && uc.CityId == favoriteCity.Id);

                Assert.NotNull(userCity);

                userFavoriteCityCount++;
            }

            var contextCount = _context.UserCities
                .Count(uc => uc.UserId == _user.Id);
            Assert.Equal(userFavoriteCityCount, contextCount);
        }
    }
}
