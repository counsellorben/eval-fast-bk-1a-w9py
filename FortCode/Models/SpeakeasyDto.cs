using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FortCode.Models
{
    public class SpeakeasyDto
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public int? CityId { get; set; }

        public string? City { get; set; }

        public string? Country { get; set; }

        public string? FavoriteDrinkName { get; set; }
    }
}