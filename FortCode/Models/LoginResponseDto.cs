namespace FortCode.Models
{
    public class LoginResponseDto
    {
        public int Id { get; set; }
        public string JwtToken { get; set; }
    }
}