namespace FortCode.Models
{
    public class SharedSpeakeasyDto
    {
        public string Name { get; set; }

        public int OtherUserCount { get; set; }
    }
}