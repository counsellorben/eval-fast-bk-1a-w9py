using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.Models
{
    public class UserSpeakeasy
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int UserId { get; set; }

        [Required]
        public int SpeakeasyId { get; set; }

        [Required]
        public string FavoriteDrinkName { get; set; }
    }
}