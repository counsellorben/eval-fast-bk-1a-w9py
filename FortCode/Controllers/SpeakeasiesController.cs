using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Helpers;
using FortCode.Models;
using FortCode.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class SpeakeasiesController : ControllerBase
    {
        private readonly ISpeakeasyRepository _speakeasyRepository;

        public SpeakeasiesController(ISpeakeasyRepository speakeasyRepository)
        {
            _speakeasyRepository = speakeasyRepository;
        }

        [HttpPost()]
        public async Task<SpeakeasyDto> AddFavorite(SpeakeasyDto speakeasy) =>
            await _speakeasyRepository.AddSpeakeasyAsync(speakeasy, (User)HttpContext.Items["User"]);

        [HttpDelete()]
        public async Task<bool> RemoveFavorite(SpeakeasyDto speakeasy) =>
            await _speakeasyRepository.RemoveSpeakeasyAsync(speakeasy, (User)HttpContext.Items["User"]);

        [HttpGet("city-favorites/{id}")]
        public async Task<IEnumerable<SpeakeasyDto>> GetCityFavorites([FromRoute]int id) =>
            await _speakeasyRepository.GetFavoriteSpeakeasiesInCityAsync(id, (User)HttpContext.Items["User"]);

        [HttpGet("shared-favorites")]
        public async Task<IEnumerable<SharedSpeakeasyDto>> GetSharedFavorites() =>
            await _speakeasyRepository.GetSharedSpeakeasiesAsync((User)HttpContext.Items["User"]);
    }
}