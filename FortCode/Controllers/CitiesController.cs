using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Helpers;
using FortCode.Models;
using FortCode.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CitiesController : ControllerBase
    {
        private readonly ICityRepository _cityRepository;

        public CitiesController(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        [HttpPost()]
        public async Task<CityDto> AddFavoriteCity(CityDto newCity) =>
            await _cityRepository.AddFavoriteCityAsync(newCity, (User)HttpContext.Items["User"]);

        [HttpDelete()]
        public async Task<bool> RemoveFavoriteCity(CityDto cityToRemove) =>
            await _cityRepository.RemoveFavoriteCityAsync(cityToRemove, (User)HttpContext.Items["User"]);

        [HttpGet("favorites")]
        public async Task<IEnumerable<CityDto>> GetFavoriteCities() =>
            await _cityRepository.GetFavoriteCitiesAsync((User)HttpContext.Items["User"]);
    }
}