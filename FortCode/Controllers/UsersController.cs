using System.Threading.Tasks;
using FortCode.Models;
using FortCode.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("add-user")]
        public async Task<IActionResult> AddUser(UserDto addUser)
        {
            var user = await _userRepository.AddUserAsync(addUser);

            if (user == null)
                return Unauthorized(new { message = "Email exists" });

            return new JsonResult(user);
        }

        [HttpPost("login-user")]
        public async Task<IActionResult> Login(LoginDto login)
        {
            var user = await _userRepository.LoginUserAsync(login);

            if (user == null)
                return Unauthorized(new { message = "Username or password is incorrect" });

            return new JsonResult(user);
        }
    }
}