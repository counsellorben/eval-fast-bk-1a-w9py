using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Repositories
{
    public class FortCodeDbContext : DbContext
    {
        public FortCodeDbContext(DbContextOptions<FortCodeDbContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<UserCity> UserCities { get; set; }

        public DbSet<Speakeasy> Speakeasies { get; set; }

        public DbSet<UserSpeakeasy> UserSpeakeasies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<City>().ToTable("Cities");
            modelBuilder.Entity<UserCity>().ToTable("UserCities");
            modelBuilder.Entity<Speakeasy>().ToTable("Speakeasies");
            modelBuilder.Entity<UserSpeakeasy>().ToTable("UserSpeakeasies");
        }
    }
}