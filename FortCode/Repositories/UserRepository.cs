using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace FortCode.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly FortCodeDbContext _context;

        private const string _unsafeSecret = "This is an UNSAFE secret for encoding our JWT";

        public UserRepository(FortCodeDbContext context)
        {
            _context = context;
        }

        public async Task<LoginResponseDto?> AddUserAsync(UserDto addUser)
        {
            if (await EmailExistsAsync(addUser.Email))
                return null;

            var salt = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            var hashedPassword = HashPassword(salt, addUser.Password);

            var user = new User
            {
                Name = addUser.Name,
                Email = addUser.Email,
                HashedPassword = hashedPassword,
                Salt = salt
            };

            var entry = await _context.AddAsync<User>(user);
            await _context.SaveChangesAsync();
            var savedUser = entry.Entity;

            return new LoginResponseDto
            {
                Id = savedUser.Id,
                JwtToken = GenerateJwtToken(savedUser)
            };
        }

        public async Task<LoginResponseDto?> LoginUserAsync(LoginDto login)
        {
            var targetUser = await _context
                .Users
                .SingleOrDefaultAsync(u => u.Email == login.Email);

            if (targetUser == null)
                return null;

            var hashedPassword = HashPassword(targetUser.Salt, login.Password);

            if (hashedPassword != targetUser.HashedPassword)
                return null;

            // authentication successful so generate jwt token
            return new LoginResponseDto
            {
                Id = targetUser.Id,
                JwtToken = GenerateJwtToken(targetUser)
            };
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await _context
                .Users
                .SingleOrDefaultAsync(u => u.Email == email);
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_unsafeSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("email", user.Email) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<bool> EmailExistsAsync(string email) => await _context
            .Users
            .AnyAsync(u => u.Email == email);

        private string HashPassword(byte[] salt, string password)
        {
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 32));
        }

        public async Task<User> ValidateJwtAndReturnUserAsync(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_unsafeSecret);
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var userEmail = jwtToken.Claims.First(x => x.Type == "email").Value;

            return await _context.Users.SingleAsync(u => u.Email == userEmail);
        }
    }
}