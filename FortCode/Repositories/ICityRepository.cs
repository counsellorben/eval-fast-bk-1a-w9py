using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models;

namespace FortCode.Repositories
{
    public interface ICityRepository
    {
        Task<CityDto> AddFavoriteCityAsync(CityDto city, User user);
        Task<bool> RemoveFavoriteCityAsync(CityDto city, User user);
        Task<IEnumerable<CityDto>> GetFavoriteCitiesAsync(User user);
    }
}