using System.Threading.Tasks;
using FortCode.Models;

namespace FortCode.Repositories
{
    public interface IUserRepository
    {
        Task<LoginResponseDto?> AddUserAsync(UserDto addUser);
        Task<bool> EmailExistsAsync(string email);
        Task<LoginResponseDto?> LoginUserAsync(LoginDto login);
        Task<User> GetUserByEmailAsync(string email);
        Task<User> ValidateJwtAndReturnUserAsync(string token);
    }
}