using System.Collections.Generic;
using System.Threading.Tasks;
using FortCode.Models;

namespace FortCode.Repositories
{
    public interface ISpeakeasyRepository
    {
        Task<SpeakeasyDto> AddSpeakeasyAsync(SpeakeasyDto speakeasy, User user);
        Task<IEnumerable<SpeakeasyDto>> GetFavoriteSpeakeasiesInCityAsync(int id, User user);
        Task<IEnumerable<SharedSpeakeasyDto>> GetSharedSpeakeasiesAsync(User user);
        Task<bool> RemoveSpeakeasyAsync(SpeakeasyDto speakeasy, User user);
    }
}