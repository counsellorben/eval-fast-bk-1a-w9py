using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Repositories
{
    public class CityRepository : ICityRepository
    {
        private readonly FortCodeDbContext _context;

        public CityRepository(FortCodeDbContext context)
        {
            _context = context;
        }

        public async Task<CityDto> AddFavoriteCityAsync(CityDto city, User user)
        {
            var existingCity = await _context.Cities
                .SingleOrDefaultAsync(c => c.Name == city.Name && c.Country == city.Country);

            if (existingCity == null)
            {
                var newCity = new City
                {
                    Name = city.Name ?? string.Empty,
                    Country = city.Country ?? string.Empty
                };
                var cityEntry = await _context.AddAsync<City>(newCity);
                await _context.SaveChangesAsync();

                existingCity = cityEntry.Entity;
            }

            var cityDto = new CityDto
            {
                Id = existingCity.Id,
                Name = existingCity.Name,
                Country = existingCity.Country
            };

            var existingUserCity = await _context.UserCities
                .SingleOrDefaultAsync(uc => uc.CityId == existingCity.Id && uc.UserId == user.Id);

            if (existingUserCity != null)
                return cityDto;

            var newUserCity = new UserCity
            {
                CityId = existingCity.Id,
                UserId = user.Id
            };
            var entry = await _context.AddAsync<UserCity>(newUserCity);
            await _context.SaveChangesAsync();
            return cityDto;
        }

        public async Task<bool> RemoveFavoriteCityAsync(CityDto city, User user)
        {
            var savedCity = await _context.UserCities
                .SingleOrDefaultAsync(c => c.UserId == user.Id && c.CityId == city.Id);

            if (savedCity == null)
                return false;

            var entry = _context.Remove<UserCity>(savedCity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<CityDto>> GetFavoriteCitiesAsync(User user)
        {
            var userCityIds = await _context.UserCities
                .Where(uc => uc.UserId == user.Id)
                .Select(uc => uc.CityId)
                .ToListAsync();
            var userCities = await _context.Cities
                .Where(c => userCityIds.Contains(c.Id))
                .ToListAsync();

            return userCities
                .Select(c => new CityDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    Country = c.Country
                });
        }
    }
}