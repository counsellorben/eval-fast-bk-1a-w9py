using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Repositories
{
    public class SpeakeasyRepository : ISpeakeasyRepository
    {
        private readonly FortCodeDbContext _context;

        public SpeakeasyRepository(FortCodeDbContext context)
        {
            _context = context;
        }

        public async Task<SpeakeasyDto> AddSpeakeasyAsync(SpeakeasyDto speakeasy, User user)
        {
            var existingSpeakeasy = await _context.Speakeasies
                .SingleOrDefaultAsync(s => s.Name == speakeasy.Name && s.CityId == speakeasy.CityId);

            if (speakeasy.CityId == null)
                return null;

            if (existingSpeakeasy == null)
            {
                var newSpeakeasy = new Speakeasy
                {
                    CityId = speakeasy.CityId ?? 0,
                    Name = speakeasy.Name ?? string.Empty,
                };
                var speakeasyEntry = await _context.AddAsync<Speakeasy>(newSpeakeasy);
                await _context.SaveChangesAsync();

                existingSpeakeasy = speakeasyEntry.Entity;
            }

            var city = await _context.Cities
                .SingleOrDefaultAsync(c => c.Id == speakeasy.CityId);

            var speakeasyDto = new SpeakeasyDto
            {
                Id = existingSpeakeasy.Id,
                Name = existingSpeakeasy.Name,
                CityId = city.Id,
                City = city.Name,
                Country = city.Country
            };

            var existingUserSpeakeasy = await _context.UserSpeakeasies
                .SingleOrDefaultAsync(us => us.UserId == user.Id && us.SpeakeasyId == existingSpeakeasy.Id);

            if (existingUserSpeakeasy != null)
            {
                speakeasyDto.FavoriteDrinkName = existingUserSpeakeasy.FavoriteDrinkName;
                return speakeasyDto;
            }

            var newUserSpeakeasy = new UserSpeakeasy
            {
                UserId = user.Id,
                SpeakeasyId = existingSpeakeasy.Id,
                FavoriteDrinkName = speakeasy.FavoriteDrinkName ?? string.Empty
            };

            var entry = await _context.AddAsync<UserSpeakeasy>(newUserSpeakeasy);
            await _context.SaveChangesAsync();

            speakeasyDto.FavoriteDrinkName = entry.Entity.FavoriteDrinkName;
            return speakeasyDto;
        }

        public async Task<bool> RemoveSpeakeasyAsync(SpeakeasyDto speakeasy, User user)
        {
            var savedSpeakeasy = await _context.UserSpeakeasies
                .SingleOrDefaultAsync(us => us.SpeakeasyId == speakeasy.Id && us.UserId == user.Id);

            if (savedSpeakeasy == null)
                return false;

            _context.Remove<UserSpeakeasy>(savedSpeakeasy);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<SpeakeasyDto>> GetFavoriteSpeakeasiesInCityAsync(int id, User user)
        {
            var city = _context.Cities.SingleOrDefault(c => c.Id == id);

            if (city == null)
                return null;

            var speakeasiesInCity = await _context.Speakeasies
                .Where(s => s.CityId == id)
                .ToListAsync();
            var speakeasiesInCityIds = speakeasiesInCity.Select(s => s.Id);

            var userSpeakeasiesInCity = await _context.UserSpeakeasies
                .Where(us => speakeasiesInCityIds.Contains(us.SpeakeasyId) && us.UserId == user.Id)
                .ToListAsync();
            var userSpeaksasiesInCityIds = userSpeakeasiesInCity.Select(s => s.Id);

            return speakeasiesInCity
                .Where(s => userSpeaksasiesInCityIds.Contains(s.Id))
                .Select(s => new SpeakeasyDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    CityId = s.CityId,
                    City = city.Name,
                    Country = city.Country,
                    FavoriteDrinkName = userSpeakeasiesInCity
                        .Single(us => us.SpeakeasyId == s.Id)
                        .FavoriteDrinkName
                });
        }

        public async Task<IEnumerable<SharedSpeakeasyDto>> GetSharedSpeakeasiesAsync(User user)
        {
            var userSpeakeasyIds = await _context.UserSpeakeasies
                .Where(us => us.UserId == user.Id)
                .Select(us => us.SpeakeasyId)
                .ToListAsync();
            var matchingSpeakeasyIds = _context.UserSpeakeasies
                .Where(us => userSpeakeasyIds.Contains(us.SpeakeasyId) && us.UserId != user.Id)
                .Select(us => us.SpeakeasyId);
            var matchingSpeakeasies = await _context.Speakeasies
                .Where(s => matchingSpeakeasyIds.Contains(s.Id))
                .ToListAsync();

            var matches = new List<SharedSpeakeasyDto>();
            foreach (var matchingSpeakeasy in matchingSpeakeasies)
            {
                matches.Add(new SharedSpeakeasyDto
                {
                    Name = matchingSpeakeasy.Name,
                    OtherUserCount = matchingSpeakeasyIds.Count(id => id == matchingSpeakeasy.Id)
                });
            }

            return matches;
        }
    }
}