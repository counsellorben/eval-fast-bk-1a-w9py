# FORT Technical Exercise Explanations
## Overview
The solution was implemented using EF Core, using a Model First approach. Testing uses the XUnit framework.

The JWT authentication code was adapted from [https://jasonwatmore.com/post/2019/10/11/aspnet-core-3-jwt-authentication-tutorial-with-example-api#app-settings-cs](https://jasonwatmore.com/post/2019/10/11/aspnet-core-3-jwt-authentication-tutorial-with-example-api#app-settings-cs).

## Running Unit Tests
In a command prompt at the root folder of this repository, enter the following:
```
dotnet test .\FortCode.Tests\FortCode.Tests.csproj
```

## Issue
The instructions in the `README.md` file regarding how to retrieve the database connection settings were incorrect. Because `Configuration.GetConnectionStrings()` reads from the `ConnectionStrings` object, the correct way to access the connection string was to use `Configuration.GetConnectionStrings("DbContext")`
