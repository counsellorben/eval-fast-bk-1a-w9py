# FORT Technical Exercise API Documentation
All `POST` bodies should use `text/json` formatting.

## Endpoints
- Users: [http://localhost:8100/users](http://localhost:8100/users)
- Cities: [http://localhost:8100/cities](http://localhost:8100/cities)
- Speakeasies: [http://localhost:8100/speakeasies](http://localhost:8100/speakeasies)

## Authentication
JWT tokens are used for authentication to access the Cities and Speakeasies endpoints.

After creating a new user or logging in, a JWT is returned in the response. To use the token, in the Headers for your request, add an `Authorization` header, with the value being the returned JWT. This will provide access to all secure endpoints.

## Users Endpoint Methods
### `Add user` 
-   ```
    POST http://localhost:8100/users/add-user
    Body:
    {
        "name": "{string}",
        "email": "{string}",
        "password": "{string}"
    }
    ```
-   ```
    Valid response:
    {
        "id": {int},
        "jwtToken": "{string}"
    }
    ```
-   No response is returned if the user already exists

### `Login user` 
-   ```
    POST http://localhost:8100/users/login-user
    Body:
    {
        "email": "{string}",
        "password": "{string}"
    }
    ```
-   ```
    Valid response:
    {
        "id": {int},
        "jwtToken": "{string}"
    }
    ```
-   No response is returned if the user does not exist

## Cities Endpoint Methods
### `Add city`
-   ```
    POST http://localhost:8100/cities
    Body:
    {
        "name": "{string}",
        "country": "{string}"
    }
    ```
-   ```
    Valid response:
    {
        "id": {int},
        "name": "{string}",
        "country": "{string}"
    }
    ```

### `Remove city`
-   ```
    DELETE http://localhost:8100/cities
    Body:
    {
        "id": {int - value returned from Add city or Favorites endpoint}
    }
    ```
-   ```
    Valid response:
    {
        true
    }
    ```
-   ```
    Response if city does not exist:
    {
        false
    }

### `Favorite cities`
-   ```
    GET http://localhost:8100/cities/favorites
    ```
-   ```
    Valid response:
    [
        {
            "id": {int},
            "name": "{string}",
            "country": "{string}"
        }
        ...
    ]
    ```

## Speakeasies Endpoint
### `Add Speakeasy`
-   ```
    POST http://localhost:8100/speakeasies
    Body:
    {
        "name": "{string}",
        "cityId": {int - value returned from Add city or city Favorites endpoint}
        "favoriteDrinkName": "{string}"
    }
    ```
-   ```
    Valid response:
    {
        "id": {int},
        "name": "{string}",
        "cityId": {int}
        "city": "{string}",
        "country": "{string}"
        "favoriteDrinkName": "{string}"
    }
    ```

### `Remove speakeasy`
-   ```
    DELETE http://localhost:8100/speakeasies
    Body:
    {
        "id": {int}
    }
    ```
-   ```
    Valid response:
    {
        true
    }
    ```
-   ```
    Response if city does not exist:
    {
        false
    }

### `Favorite Speakeasies in City`
-   ```
    GET http://localhost:8100/speakeasies/city-favorites/{city id}
-   ```
    Valid response:
    [
        {
            "id": {int},
            "name": "{string}",
            "cityId": {int}
            "city": "{string}",
            "country": "{string}"
            "favoriteDrinkName": "{string}"
        }
        ...
    ]
    ```

### Shared Favorite Speakeasies
-   ```
    GET http://localhost:8100/speakeasies/shared-favorites
-   ```
    Valid response:
    [
        {
            "name": "{string}",
            "otherUsersCount": {int}
        }
        ...
    ]
    ```
